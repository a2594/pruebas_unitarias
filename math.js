export const suma = (a, b) => {
    return a+b;
}

export const resta = (a, b) => {
    return a-b;
}

export const checkInteger = (value) => {
    if(value % 1 === 0){
        return true;
    }
    return false;
}