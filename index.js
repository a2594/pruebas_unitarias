import path, {dirname} from "path";
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
import {resta, suma} from "./math.js";


console.log(path.resolve("package.json"));
console.log(suma(3,4));
console.log(resta(2,1));
console.log(__dirname, __filename);